package se.olapetersson.pipeline.app.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldResource {

    @RequestMapping("/")
    public String index() {
        return "Hello, pipeline!";
    }

}
